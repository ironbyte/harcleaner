package testutils

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
)

// Generates random alphanumeric strings
// Based on hexadecimal
func randomHex(n int) (string, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

// Generates random and junk strings that
// look like JWT tokens
func GenerateJwtString() string {
	header := []byte(`{"alg":"lol", "type":"jwt"}`)
	payload_s, _ := randomHex(10)
	payload := []byte(`{"message":` + payload_s + `}`)
	signature_s, _ := randomHex(10)
	signature := []byte(signature_s)
	jwt := base64.URLEncoding.EncodeToString(header) + "." +
		base64.URLEncoding.EncodeToString(payload) + "." +
		base64.URLEncoding.EncodeToString(signature)
	return jwt
}

// Generate random and junk strings that
// look like GitLab tokens
func GenerateGitLabToken() string {

	n := 10
	token_string, _ := randomHex(n)
	return "glpat" + "-" + token_string
}
