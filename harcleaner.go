package main

import (
	"flag"
	"os"

	"harcleaner/harprocessor"
	"harcleaner/logging"
)

func main() {
	// Command line arguments
	input_file := flag.String("input", "", "Path to input file")
	output_file := flag.String("output", "clean.har", "Path to output file")
	debug_flag := flag.Bool("debug", false, "Enable debug logging")
	flag.Parse()

	// Set debug global variable
	if *debug_flag {
		logging.SetDebug()
	}

	// Check if arguments are provided
	if *input_file == "" || *output_file == "" {
		flag.Usage()
		logging.Log().Fatalln("Required arguments not provided")
	}

	// Read input file
	logging.Log().Infof("Reading input file: %s", *input_file)
	content, err := os.ReadFile(*input_file)
	if err != nil {
		logging.Log().Fatalf("Error when opening file: %s", err.Error())
	}

	// Unmarshall the data into the `Har` structure
	logging.Log().Infoln("Parsing and validating input data")
	har, err := harprocessor.UnmarshallHar(content)
	if err != nil {
		logging.Log().Fatalf("Error during JSON Unmarshal: %s", err.Error())
	}

	// Process HAR
	logging.Log().Infoln("Cleaning HAR data")
	metrics, err := har.Process()
	if err != nil {
		logging.Log().Fatalf("Error during HAR processing: %s", err.Error())
	}

	logging.Log().Debugf("Metrics: %+v", metrics)

	// Exit with an error if there were no redactions
	if metrics.Redactions == 0 {
		logging.Log().Fatalln("Exiting early because no redactions were made. Output file will not be written")
	}

	// Convert resulting data structure to JSON
	logging.Log().Infoln("Converting processed data to JSON")
	json, err := harprocessor.MarshallHar(har)
	if err != nil {
		logging.Log().Fatalf("Error during JSON Marshal: %s", err.Error())
	}

	// Create an output file
	logging.Log().Debugf("Creating output file: %s", *output_file)
	of, err := os.Create(*output_file)
	if err != nil {
		logging.Log().Fatalf("Error during creation of output file: %s", err.Error())
	}

	defer of.Close()

	// Write JSON to file
	logging.Log().Debugf("Writing data to output file: %s", *output_file)
	ob, err := of.Write(json)
	if err != nil {
		logging.Log().Fatalf("Error writing to output file: %s", *output_file)
	}

	logging.Log().Infof("%d bytes written to output file: %s ", ob, *output_file)
}
