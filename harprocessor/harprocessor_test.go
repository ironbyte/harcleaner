package harprocessor

import (
	"harcleaner/testutils"
	"os"
	"strings"
	"testing"
)

func TestUnmarshalHar(t *testing.T) {
	test_file := "../testdata/minimal.har"
	b, err := os.ReadFile(test_file)
	if err != nil {
		t.Fatal(err)
	}

	_, err = UnmarshallHar(b)
	if err != nil {
		t.Fatal(err)
	}
}

func TestProcessPostDataParam(t *testing.T) {
	mock := PostDataParam{
		Name:  "postdataparam-name",
		Value: "sensitive",
	}

	ProcessPostDataParam(&mock)

	if mock.Value != replace_string {
		t.Errorf("Expected Value to be redacted")
	}
}

func TestProcessPostData(t *testing.T) {
	mock := PostData{
		MimeType: "text/html",
		Text:     "sensitive",
	}

	ProcessPostData(&mock)

	if mock.MimeType != replace_string {
		t.Errorf("Expected MimeType to be redacted")
	}

	if mock.Text != replace_string {
		t.Errorf("Expected Text to be redacted")
	}
}

func TestProcessResponseContent(t *testing.T) {
	mock := Content{
		Size:     123,
		MimeType: "text/html",
		Text:     "sensitive",
		Encoding: "",
	}

	ProcessResponseContent(&mock)

	if mock.Text != replace_string {
		t.Errorf("Expected Text to be redacted")
	}
}

func TestProcessCookie(t *testing.T) {
	mock := Cookie{
		Name:  "cookie-name",
		Value: "sensitive",
	}

	ProcessCookie(&mock)

	if mock.Value != replace_string {
		t.Errorf("Expected cookie value to be redacted")
	}
}

func TestProcessStringRegex(t *testing.T) {
	testCases := []struct {
		input    string
		expected string
	}{
		{
			"my_token=sensitive&hello=goodbye&code=sensitive",
			"my_token=" + replace_string + "&hello=goodbye&code=" + replace_string,
		},
		{
			testutils.GenerateJwtString(),
			replace_string,
		},
		{
			testutils.GenerateGitLabToken(),
			replace_string,
		},
		{
			"https://" + "my_awesome_user123" + ":" + "sensitive" + "@mygitserver.xyz/my-cool-project.git",
			"https://REDACTED:REDACTED@mygitserver.xyz/my-cool-project.git",
		},
	}

	for _, tc := range testCases {
		output := ProcessStringRegex(tc.input)

		if output != tc.expected {
			t.Errorf("Expected %s, got %s", tc.expected, output)
		}

		if strings.Contains(output, "sensitive") {
			t.Errorf("Sensitive string not redacted as expected: %s, got %s", tc.expected, output)
		}
	}
}

func TestWalkEntry(t *testing.T) {
	mock := Entry{
		Request: Request{
			Headers: []Header{
				{Name: "Auth", Value: "sensitive"},
				{Name: "Cookie", Value: "sensitive"},
			},
		},
		Response: Response{
			Content: Content{
				Text:     "<html>sensitive</html>",
				MimeType: "text/html",
			},
		},
	}

	WalkEntry(&mock, nil)

	if mock.Request.Headers[0].Value != replace_string {
		t.Errorf("Expected header value to be redacted")
	}

	if mock.Response.Content.Text != replace_string {
		t.Errorf("Expected content text to be redacted")
	}
}

func TestMarshallHar(t *testing.T) {

	mock := Har{
		Log: Log{
			Entries: []Entry{
				{Request: Request{Method: "GET"}},
			},
		},
	}

	json, err := MarshallHar(mock)
	if err != nil {
		t.Fatal(err)
	}

	if !strings.Contains(string(json), `"method": "GET"`) {
		t.Errorf("Expected JSON to contain GET method")
	}

}

func TestUnmarshallHar(t *testing.T) {

	jsonData := []byte(`{
        "log": {
            "entries": [
                {"request": {"method": "GET"}}
            ]
        }
    }`)

	har, err := UnmarshallHar(jsonData)
	if err != nil {
		t.Fatal(err)
	}

	if har.Log.Entries[0].Request.Method != "GET" {
		t.Errorf("Expected GET method in unmarshalled data")
	}

}

func TestProcess(t *testing.T) {

	mock := Har{
		Log: Log{
			Entries: []Entry{
				{
					Request: Request{
						Headers: []Header{
							{Name: "Auth", Value: "token123"},
						},
					},
				},
			},
		},
	}

	metrics, err := mock.Process()
	if err != nil {
		t.Fatal(err)
	}

	if mock.Log.Entries[0].Request.Headers[0].Value != replace_string {
		t.Errorf("Expected header value to be redacted")
	}

	if metrics.Redactions == 0 {
		t.Errorf("Expected redactions above zero in returned metrics")
	}

}
