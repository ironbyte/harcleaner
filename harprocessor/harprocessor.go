package harprocessor

import (
	"encoding/json"
	"reflect"
	"regexp"
	"strings"
)

var replace_string = "REDACTED"

// Creating structs to define data structure of marshalled JSON
// Approximates the HAR file's data format/structure
type Har struct {
	Log Log `json:"log"`
}

type Log struct {
	Version string  `json:"version"`
	Creator Creator `json:"creator"`
	Pages   []Page  `json:"pages"`
	Entries []Entry `json:"entries"`
}

type Creator struct {
	Name    string `json:"name"`
	Version string `json:"version"`
	Comment string `json:"comment"`
}

type Page struct {
	StartedDateTime string     `json:"startedDateTime"`
	Id              string     `json:"id"`
	Title           string     `json:"title"`
	PageTimings     PageTiming `json:"pageTimings"`
}

type PageTiming struct {
	OnContentLoad float64 `json:"onContentLoad"`
	OnLoad        float64 `json:"onLoad"`
}

type Entry struct {
	Request         Request     `json:"request"`
	Response        Response    `json:"response"`
	ServerIpAddress string      `json:"serverIpAddress"`
	StartedDateTime string      `json:"startedDateTime"`
	Time            float64     `json:"time"`
	Timings         EntryTiming `json:"timings"`
}

type Request struct {
	Method      string        `json:"method"`
	Url         string        `json:"url"`
	HttpVersion string        `json:"httpVersion"`
	Headers     []Header      `json:"headers"`
	QueryString []QueryString `json:"queryString"`
	Cookies     []Cookie      `json:"cookies"`
	HeadersSize int           `json:"headersSize"`
	BodySize    int           `json:"bodySize"`
	PostData    PostData      `json:"postData"`
}

type Response struct {
	Status      int      `json:"status"`
	StatusText  string   `json:"statusText"`
	HttpVersion string   `json:"httpVersion"`
	Headers     []Header `json:"headers"`
	Cookies     []Cookie `json:"cookies"`
	Content     Content  `json:"content"`
	RedirectURL string   `json:"redirectURL"`
	HeadersSize int      `json:"headersSize"`
	BodySize    int      `json:"bodySize"`
}

type Header struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type Cookie struct {
	Name     string `json:"name"`
	Value    string `json:"value"`
	Path     string `json:"path"`
	Domain   string `json:"domain"`
	Expires  string `json:"expires"`
	HttpOnly bool   `json:"httpOnly"`
	Secure   bool   `json:"secure"`
	SameSite string `json:"sameSite"`
}

type QueryString struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type Content struct {
	Size     int    `json:"size"`
	MimeType string `json:"mimeType"`
	Text     string `json:"text"`
	Encoding string `json:"encoding"`
}

type EntryTiming struct {
	Blocked         float64 `json:"blocked"`
	Dns             float64 `json:"dns"`
	Ssl             float64 `json:"ssl"`
	Connect         float64 `json:"connect"`
	Send            float64 `json:"send"`
	Wait            float64 `json:"wait"`
	Receive         float64 `json:"receive"`
	BlockedQueueing float64 `json:"_blocked_queueing"`
}

type PostData struct {
	MimeType string          `json:"mimeType"`
	Text     string          `json:"text"`
	Params   []PostDataParam `json:"params"`
}

type PostDataParam struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type RegexPatternReplace struct {
	Description     string
	Regex           string
	Replacement     string
	ContainsKeyword bool
}

var KeywordList = []string{
	"Authorization",
	"SAMLRequest",
	"SAMLResponse",
	"access_token",
	"appID",
	"assertion",
	"auth",
	"authenticity_token",
	"challenge",
	"client_id",
	"client_secret",
	"code",
	"code_challenge",
	"code_verifier",
	"email",
	"facetID",
	"fcParams",
	"id_token",
	"password",
	"refresh_token",
	"serverData",
	"shdf",
	"state",
	"token",
	"usg",
	"vses2",
	"x-client-data",
	"passwd",
	"Cookie",
}

type Metrics struct {
	Entries          int
	Strings          int
	RegexComparisons int
	Redactions       int
}

var metrics Metrics

// Unmarshall the data into the `Har` structure
func UnmarshallHar(b []byte) (Har, error) {
	var har Har
	err := json.Unmarshal(b, &har)
	if err != nil {
		return har, err
	}

	return har, nil
}

// Marhsall data from `Har` to JSON []byte
func MarshallHar(har Har) ([]byte, error) {
	b, err := json.MarshalIndent(har, "", "  ")
	if err != nil {
		return b, err
	}

	return b, nil
}

// Function to process HAR struct
func (h *Har) Process() (Metrics, error) {

	// Add number of entries to metrics
	metrics.Entries = len(h.Log.Entries)

	// For each entry in the HAR file
	// Walk the Entry struct to process
	// its fields
	for i := 0; i < len(h.Log.Entries); i++ {
		WalkEntry(&h.Log.Entries[i], nil)
	}

	// Add comment to HAR to identify this file was processed
	h.Log.Creator.Comment = "Processed by harcleaner"

	// Will implement error handling later
	return metrics, nil
}

func WalkEntry(st interface{}, parent_type reflect.Type) {
	val := reflect.ValueOf(st)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}

	current_type := val.Type()

	// Process whole struct
	switch val.Kind().String() {
	case "struct":
		ProcessStruct(st)
	}

	// Iterate over struct fields
	for i := 0; i < val.NumField(); i++ {
		f := val.Field(i)
		switch f.Kind() {
		case reflect.Struct:
			// Walk into struct
			WalkEntry(f.Addr().Interface(), current_type)
		case reflect.Slice:
			// Walk into each item in slice
			for j := 0; j < f.Len(); j++ {
				WalkEntry(f.Index(j).Addr().Interface(), current_type)
			}
		case reflect.String:
			// Process each string field
			ProcessString(val.Field(i))
		}
	}
}

func ProcessStruct(st interface{}) {
	struct_type := reflect.TypeOf(st).String()

	s := reflect.ValueOf(st).Elem()
	name := s.FieldByName("Name").String()

	// Regardless of the struct's type,
	// if the struct's Name field
	// matches any keywords, redact
	// the Value field
	match := KeywordMatch(name)
	if match {
		value := s.FieldByName("Value")
		Redact(value, "")
	}

	// Process the struct based on
	// its type
	if strings.Contains(struct_type, "Cookie") {
		ProcessCookie(st)
	} else if strings.Contains(struct_type, "Content") {
		ProcessResponseContent(st)
	} else if strings.Contains(struct_type, "PostDataParam") {
		ProcessPostDataParam(st)
	} else if strings.Contains(struct_type, "PostData") {
		ProcessPostData(st)
	}
}

func Redact(v reflect.Value, replace string) {
	if replace == "" {
		replace = "REDACTED"
	}

	v.SetString(replace)

	metrics.Redactions += 1

}

func KeywordMatch(s string) bool {

	keywords := strings.Join(KeywordList, "|")
	re := regexp.MustCompile(`(?i)(` + keywords + `)`)
	match := re.MatchString(s)
	return match
}

func ProcessString(v reflect.Value) {
	result := ProcessStringRegex(v.String())

	if v.String() != result {
		Redact(v, result)
	}

	metrics.Strings += 1

}

func RegexPatternReplaceList() []RegexPatternReplace {
	patterns := []RegexPatternReplace{
		RegexPatternReplace{
			Description:     `Redact value in keyword=value`,
			Regex:           `(keyword)(=){1}(.+?)(&|\?|$)`,
			Replacement:     `${1}${2}` + replace_string + `${4}`,
			ContainsKeyword: true,
		},
		RegexPatternReplace{
			Description:     `Redact JWTs`,
			Regex:           `(ey[A-Za-z0-9-_=]+)\.(ey[A-Za-z0-9-_=]+)\.[A-Za-z0-9-_.+/=]+`,
			Replacement:     replace_string,
			ContainsKeyword: false,
		},
		RegexPatternReplace{
			Description:     `Redact GitLab Tokens`,
			Regex:           `\b(glpat|glptt|GR1348941|glrt|glft|gldt|glsoat|glcbt)-[0-9a-zA-Z_\-]{20}\b`,
			Replacement:     replace_string,
			ContainsKeyword: false,
		},
		RegexPatternReplace{
			Description:     `Redact user:pass in URL`,
			Regex:           `(:\/\/){1}([^\:]+)(\:){1}([^\@]+)(\@){1}`,
			Replacement:     `${1}` + replace_string + `${3}` + replace_string + `${5}`,
			ContainsKeyword: false,
		},
	}

	return patterns
}

func ProcessStringRegex(s string) string {

	src_string := s

	patterns := RegexPatternReplaceList()
	for _, p := range patterns {
		// Break from loop if string is already
		// either empty or fully redacted
		if src_string == "" || src_string == replace_string {
			break
		}

		regex_pattern := p.Regex

		// Only process keywords if the pattern supports it
		if p.ContainsKeyword {
			// Join all keywords in a regex "OR"
			keywords_or := strings.Join(KeywordList[:], "|")
			regex_pattern = strings.ReplaceAll(p.Regex, "keyword", keywords_or)
		}

		re := regexp.MustCompile(regex_pattern)
		result := re.ReplaceAllString(src_string, p.Replacement)
		metrics.RegexComparisons += 1

		// Increment redactions metric if string has been altered
		if result != src_string {
			metrics.Redactions += 1
		}

		src_string = result
	}

	return src_string
}

func ProcessCookie(st interface{}) error {
	s := reflect.ValueOf(st).Elem()
	value := s.FieldByName("Value")

	// Redact all cookies regardless of value
	Redact(value, "")

	return nil
}

func ProcessResponseContent(st interface{}) error {
	s := reflect.ValueOf(st).Elem()
	value := s.FieldByName("Text")
	mimetype := s.FieldByName("MimeType").String()

	mimetypes := []string{
		"text/html",
		"application/javascript",
		"text/javascript",
	}

	// Redact response content if the mimetype matches the list of
	// mimetypes
	for _, m := range mimetypes {
		if strings.Contains(m, mimetype) {
			Redact(value, "")
			break
		}
	}

	return nil

}

func ProcessPostData(st interface{}) {
	s := reflect.ValueOf(st).Elem()
	mimetype := s.FieldByName("MimeType")
	value := s.FieldByName("Text")

	if mimetype.String() != "" {
		Redact(mimetype, "")
	}

	if value.String() != "" {
		Redact(value, "")
	}
}

func ProcessPostDataParam(st interface{}) {
	s := reflect.ValueOf(st).Elem()

	// Redact the value of all PostDataParam
	value := s.FieldByName("Value")
	Redact(value, "")
}
